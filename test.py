from random import *
import numpy as np

global taille 
#--------------------initialise le laby avec des murs 1 fois sur 2 colonne et ligne 
def init_maze(width, height):
    nb = 0
    maze = []
    wall = []
    line = []
    for i in range(0, taille):
        wall.append(-1)
        if i%2 == 1 :
          line.append(0)
        else:
          line.append(-1)

    for i in range(0,taille):
      if i%2 == 0:
        maze.append(wall)
      else:
        maze.append(line)
    maze = np.asarray(maze)
    for i in range(0,width-1):
      for j in range(0,height-1):
        if maze[i][j] == 0:
          nb = nb +1
          maze[i][j] = nb
    
    maze[1][0] = 0
    maze[width-3][height-2]  = nb
    print_maze(maze,width,height)
   
    #print_maze(maze)
    randomTotal(maze,width,height)
#-------------------------- verifie si le tableau contien le mombre
def fini(maze,w,h):
  for i in range (1,w-1,2):
        for j in range (1,h - 1,2):
          if maze[i][j] != maze[1][1]:
              return False
  return True




#--------------------------- affiche le laby
def print_maze(maze,w,h):
    for i in range(0, w-1):
        for j in range(0, h-1):
                if maze[i][j] == -1:
                  print("🈴",end="")
                else:
                  print("🚺",end="")
        print('\n')
    print('\n')
#---------------------------- créa un laby avec un chemin unique 
def randomTotal(maze,w,h):
    
  while fini(maze,w,h) == False:
    x = randint(0,(w-4))+1
    y = 0
    cell1 = None
    cell2 = None 
    if  x%2 == 0:
      y = randint(0,(h-4)//2) *2 +1
      
    else:
      y = randint(0,(h-5)//2) *2 +2
      
    
    if maze[x-1][y] == -1:
      cell1 = maze[x][y-1]
      cell2 = maze[x][y+1]
    else:
      cell1 = maze[x-1][y]
      cell2 = maze[x+1][y]
    print(cell1)
    print(cell2)
    print("\n")
    if cell1 != cell2:
      maze[x][y] = 0
      for i in range (1,w-1,2):
        for j in range (1,h-1,2):
           if maze[i][j] == cell2:
               maze[i][j] = cell1
  alea(maze,w,h)
  return maze

#---------------------------------- crée un laby avec des troue au hazard 
def alea(maze,w,h):
    for i in range(0, w-1):
        x = randint(0,(w-4))+1
        for j in range(0, h-1):
              if  x%2 == 0:
                y = randint(0,(h-1)//2) *2 +1
              else:
                y = randint(0,(h-5)//2) *2 +2
        maze[x][y]= 0
    print_maze(maze,w,h)

x = 20
y = 20



taille = (x*y)
init_maze(x,y)
